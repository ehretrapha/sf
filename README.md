### Author 
 Raphael EHRET
### Name 
 sf (size files/folders)
### Mail 
 ehretrapha[at]eisti[dot]eu
### Langage 
 python 3
### Type 
 script

### Description 
 prints the summed size of the content of each folder located
                on the root of the current directory. By default, the results
                are sorted by ascending order on the name. For other sort options,
                see the [Arguments] part.  

### Required 

* hurry.filesize python library 

### Arguments 

    [-f] : also prints the size of the files at the root of the path
    [-H] : also prints the size of hidden folders (and file if you actived -f)
    [-s] : sorts the results by ascending order on the size
    [-S] : sorts the results by descending order on the size
